#!/usr/bin/env python

import marshal, sys
import os,pprint,itertools

splitme = lambda x: list(itertools.islice(x,1,None))
first   = lambda s:s[0]
idn     = lambda s:s

def dirchg(a=None, k=idn, func = os.listdir, d = dict(), record = dict()):
    
    s = None

    # === file persistence, or return in-memory record
    if a is None:
        if isinstance(d, list) and tuple(map(type,d)) == (file,): #--> LOAD
            print >> sys.stderr, "loading record"
            record.update(marshal.load(*d))
        elif isinstance(record, list):                            
            tp = tuple(map(type,record))
            if tp == (dict,): d.update(                         #--> ASSIGN
                                { k: { f for f,i in v.items() if i }
                                  for k,v in record[0].items() } )
            elif tp == (dict, file):                              #--> SAVE
                print >> sys.stderr, "saving record"
                marshal.dump(*record)
        return record                                          # ==> RETURN

    if a and not isinstance(a, str): raise RuntimeError( "first arg not str or NoneType" )

    if a == "" :
        if k is not idn:
            raise RuntimeError( "idn must be used with '' arg" )
        if d: a=d.keys()[0]
        else: return 

    entries = None

    if a:
        procd = func(a)
        a = k(a)
        s = d.get(a)
        if s is None: s = d[a] = set()
        t = set(s)
        s.clear()
        s.update(procd)
        if s != t:
            pprint.pprint( s)
        s_d = {k:1 for k in s}
        entries = record.get(a) or dict() 
        entries.update( s_d )
        e_k = entries.keys()
        for i in e_k:
            if i not in s: entries[i] = 0
        record[a] = entries

    #return entries

if __name__ == '__main__':

    try:
        input_raw = raw_input
    except:
        input_raw = input

    input_file = open('/tmp/stuff','rb')
    L = dirchg(d=[input_file]) ; input_file.close() # ; print L
    a = dirchg(record=[L]) # cache it
    b = dirchg('')

    q = None
    try:
        while not q:
            q = input_raw('->')
            b = dirchg('')
    except EOFError: 
            sys.exit(0)
    finally:
        rec = dirchg()
        output_file = open('/tmp/stuff','wb')
        dirchg(record=[rec,output_file])
        print(repr(rec))
